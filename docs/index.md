---
layout: home

hero:
    name:    nano/modules/openapi
    tagline: openapi module for nano
    actions:
        -   theme: brand
            text:  get started
            link:  /getting-started/introduction

features:
    -   title:   simple and minimal, always
        details: |
                 nano is a simple and minimal framework, and so are its modules. nano/modules/openapi is a openapi generator for nano
    -   title:   openapi
        details: |
                 nano/modules/openapi provides a simple way to generate openapi documentation for your nano application
---
