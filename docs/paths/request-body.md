---
layout: doc
---

<script setup>
const args = {
    Body: [
        { name: 'class', type: 'string' },
        { name: 'ref', type: 'bool' },
    ],
};
</script>

##### laylatichy\nano\modules\openapi\requests\Body

## usage

## <Types fn="Body" r="void" :args="args.Body" /> {#Body}

to create a request body just point to the class name

```php
class User {
    public string $name;
}

useRouter()->post('/users',
    #[Body(User::class)]
    fn(Request $request): Response => useResponse()
);
```

will generate

```yaml
paths:
    /users:
        post:
            summary:     '/users'
            operationId: post-users
            requestBody:
                content:
                    application/json:
                        schema:
                            type: object
                            properties:
                                name:
                                    type: string
                            required:
                                - name
```





