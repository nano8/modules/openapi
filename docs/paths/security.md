---
layout: doc
---

<script setup>
const args = {
    Security: [
        { name: 'value', type: 'string' },
    ],
};
</script>

##### laylatichy\nano\modules\openapi\paths\Security

## usage

## <Types fn="Security" r="void" :args="args.Security" /> {#Security}

```php
useRouter()->get('/users',
    #[Security('jwt')]
    fn(Request $request): Response => useResponse()
);
```

will generate

```yaml
paths:
    /users:
        get:
            summary:     '/users'
            operationId: get-users
            security:
                -   jwt: [ ]
```





