---
layout: doc
---

<script setup>
const args = {
    Response: [
        { name: 'httpCode', type: 'HttpCode' },
        { name: 'ref', type: 'string' },
        { name: 'repository', type: 'bool' },
        { name: 'nullable', type: 'bool' },
    ],
    ResponseOK: [
        { name: 'ref', type: 'string' },
        { name: 'repository', type: 'bool' },
        { name: 'nullable', type: 'bool' },
    ],
    ResponseBadRequest: [],
    ResponseUnauthorized: [],
};
</script>

##### laylatichy\nano\modules\openapi\requests\Response

## usage

## <Types fn="Response" r="void" :args="args.Response" /> {#Response}

```php
class User {
    public string $name;
}

useRouter()->get('/users/1',
    #[Response(HttpCode::OK, User::class)]
    fn(Request $request): Response => useResponse()
);
```

will generate

```yaml
paths:
    /users:
        get:
            summary:     '/users/1'
            operationId: get-users-1
            responses:
                200:
                    content:
                        application/json:
                            schema:
                                type: object
                                properties:
                                    response:
                                        oneOf:
                                            -   $ref: '#/components/schemas/User'
                                            -   type: 'null'
```

## with collection repository

```php
class User {
    public string $name;
}

useRouter()->get('/users',
    #[Response(HttpCode::OK, User::class, true)]
    fn(Request $request): Response => useResponse()
);
```

will generate

```yaml
paths:
    /users:
        get:
            summary:     '/users'
            operationId: get-users
            responses:
                200:
                    content:
                        application/json:
                            schema:
                                type: object
                                properties:
                                    response:
                                        type: object
                                        properties:
                                            data:
                                                type: array
                                                items:
                                                    $ref: '#/components/schemas/User'
                                            prev:
                                                type: boolean
                                            next:
                                                type: boolean
```

## <Types fn="ResponseOK" r="void" :args="args.ResponseOK" /> {#ResponseOK}

```php
class User {
    public string $name;
}

useRouter()->get('/users/1',
    #[ResponseOK(User::class)]
    fn(Request $request): Response => useResponse()
);
```

will generate

```yaml
paths:
    /users:
        get:
            summary:     '/users/1'
            operationId: get-users-1
            responses:
                200:
                    content:
                        application/json:
                            schema:
                                type: object
                                properties:
                                    response:
                                        oneOf:
                                            -   $ref: '#/components/schemas/User'
                                            -   type: 'null'
```

## <Types fn="ResponseBadRequest" r="void" :args="args.ResponseBadRequest" /> {#ResponseBadRequest}

```php
useRouter()->get('/users',
    #[ResponseBadRequest]
    fn(Request $request): Response => useResponse()
);
```

will generate

```yaml
paths:
    /users:
        get:
            summary:     '/users'
            operationId: get-users
            responses:
                400:
                    content:
                        application/json:
                            schema:
                                type: object
                                properties:
                                    response:
                                        type:    string
                                        example: 'bad Request'
```

## <Types fn="ResponseUnauthorized" r="void" :args="args.ResponseUnauthorized" /> {#ResponseUnauthorized}

```php
useRouter()->get('/users',
    #[ResponseUnauthorized]
    fn(Request $request): Response => useResponse()
);
```

will generate

```yaml
paths:
    /users:
        get:
            summary:     '/users'
            operationId: get-users
            responses:
                401:
                    content:
                        application/json:
                            schema:
                                type: object
                                properties:
                                    response:
                                        type:    string
                                        example: 'unauthorized'
```





