---
layout: doc
---

<script setup>
const args = {
    Summary: [
        { name: 'value', type: 'string' },
        { name: 'description', type: 'string' },
    ],
};
</script>

##### laylatichy\nano\modules\openapi\paths\Summary

## usage

## <Types fn="Summary" r="void" :args="args.Summary" /> {#Summary}

```php
useRouter()->get('/test',
    #[Summary('this is the test endpoint', 'this is the description')]
    fn(Request $request): Response => useResponse()
);
```

will generate

```yaml
paths:
    /test:
        get:
            summary:     'this is the test endpoint'
            description: 'this is the description'
            operationId: get-test
```
