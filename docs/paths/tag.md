---
layout: doc
---

<script setup>
const args = {
    Tag: [
        { name: 'value', type: 'string' },
    ],
};
</script>

##### laylatichy\nano\modules\openapi\paths\Tag

## usage

## <Types fn="Tag" r="void" :args="args.Tag" /> {#Tag}

```php
useRouter()->get('/users',
    #[Tag('users')]
    fn(Request $request): Response => useResponse()
);
```

will generate

```yaml
paths:
    /users:
        get:
            summary:     '/users'
            operationId: get-users
            tags:
                - users
```





