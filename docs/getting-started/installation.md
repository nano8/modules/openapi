---
layout: doc
---

## installation

you can install this module using composer

```sh
composer require laylatichy/nano-modules-openapi
```

## registering module

```php
use laylatichy\nano\modules\openapi\OpenApiModule;

useNano()->withModule(new OpenApiModule());

// define your handlers here or in a .config/openapi.php file
```
