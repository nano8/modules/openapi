import { defineConfig } from 'vitepress';

export default defineConfig({
    lastUpdated:   true,
    sitemap:       {
        hostname: 'https://nano8.gitlab.io/modules/openapi/',
    },
    title:         'nano/modules/openapi',
    titleTemplate: 'nano/modules/openapi - documentation',
    description:   'OpenApi module for nano framework',
    base:          '/modules/openapi/',
    themeConfig:   {
        lastUpdated: {
            text:          'updated',
            formatOptions: {
                dateStyle: 'long',
                timeStyle: 'medium',
            },
        },
        search:      {
            provider: 'local',
        },
        nav:         [],
        sidebar:     [
            {
                text:  'getting started',
                items: [
                    {
                        text: 'introduction',
                        link: '/getting-started/introduction',
                    },
                    {
                        text: 'installation',
                        link: '/getting-started/installation',
                    },
                ],
            },
            {
                text:  'usage',
                items: [
                    {
                        text: 'defineOpenApiInfo',
                        link: '/usage/define-openapi-info',
                    },
                    {
                        text: 'defineOpenApiServer',
                        link: '/usage/define-openapi-server',
                    },
                    {
                        text: 'defineOpenApiTag',
                        link: '/usage/define-openapi-tag',
                    },
                    {
                        text: 'defineOpenApiSecurityScheme',
                        link: '/usage/define-openapi-security-scheme',
                    },
                ],
            },
            {
                text:  'schemas',
                items: [
                    {
                        text: 'Schema',
                        link: '/schemas/schema',
                    },
                    {
                        text: 'Example',
                        link: '/schemas/example',
                    },
                    {
                        text: 'Format',
                        link: '/schemas/format',
                    },
                    {
                        text: 'ReadonlyProperty',
                        link: '/schemas/readonly-property',
                    },
                    {
                        text: 'Ref',
                        link: '/schemas/ref',
                    },
                    {
                        text: 'Required',
                        link: '/schemas/required',
                    },
                    {
                        text: 'Tag',
                        link: '/schemas/tag',
                    },
                ],
            },
            {
                text:  'paths',
                items: [
                    {
                        text: 'Summary',
                        link: '/paths/summary',
                    },
                    {
                        text: 'Tag',
                        link: '/paths/tag',
                    },
                    {
                        text: 'Security',
                        link: '/paths/security',
                    },
                    {
                        text: 'Request Body',
                        link: '/paths/request-body',
                    },
                    {
                        text: 'Response',
                        link: '/paths/response',
                    },
                ],
            },
            {
                text: 'nano documentation',
                link: 'https://nano.laylatichy.com',
            },
        ],
        socialLinks: [
            {
                icon: 'github',
                link: 'https://gitlab.com/nano8/modules/openapi',
            },
        ],
        editLink:    {
            text:    'edit this page',
            pattern: 'https://gitlab.com/nano8/modules/openapi/-/edit/dev/docs/:path',
        },
    },
});
