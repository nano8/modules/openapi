---
layout: doc
---

<script setup>
const args = {
    Required: [
        { name: 'value', type: 'string' },
    ],
};
</script>

##### laylatichy\nano\modules\openapi\components\schemas\Required

## usage

required on non-nullable properties is automatically added to the schema

## <Types fn="Required" r="void" :args="args.Required" /> {#Required}

```php
#[Schema('user model', 'user model description')]
class User {
    public string $name;
}
```

will generate

```yaml
components:
    schemas:
        User:
            type:        object
            title:       user model
            description: user model description
            properties:
                name:
                    type: string
            required:
                - id
```

to add required to nullable properties, use the `Required` attribute

```php
#[Schema('user model', 'user model description')]
class User {
    #[Required]
    public ?string $name;
}
```

will generate

```yaml
components:
    schemas:
        User:
            type:        object
            title:       user model
            description: user model description
            properties:
                name:
                    type:     string
                    Required: true
            required:
                - id
```

to remove required from non-nullable properties, use the `Required` attribute

```php
#[Schema('user model', 'user model description')]
class User {
    #[Required(false)]
    public string $name;
}
```

will generate

```yaml
components:
    schemas:
        User:
            type:        object
            title:       user model
            description: user model description
            properties:
                name:
                    type: string
```

