---
layout: doc
---

<script setup>
const args = {
    Format: [
        { name: 'value', type: 'string' },
    ],
};
</script>

##### laylatichy\nano\modules\openapi\components\schemas\Format

## usage

## <Types fn="Format" r="void" :args="args.Format" /> {#Format}

```php
#[Schema('user model', 'user model description')]
class User {
    #[Format('int64')]
    public int $id;
}
```

will generate

```yaml
components:
    schemas:
        User:
            type:        object
            title:       user model
            description: user model description
            properties:
                id:
                    type:   integer
                    format: int64
            required:
                - id
```

