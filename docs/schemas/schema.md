---
layout: doc
---

<script setup>
const args = {
    Schema: [
        { name: 'title', type: 'string' },
        { name: 'description', type: 'string' },
    ],
};
</script>

##### laylatichy\nano\modules\openapi\components\schemas\Schema

## usage

## <Types fn="Schema" r="void" :args="args.Schema" /> {#Schema}

```php
#[Schema('contact model', 'contact model description')]
class Contact {
    public string $phone;
}

#[Schema('user model', 'user model description')]
class User {
    public string $name;

    public ?string $city;
    
    public Contact $contact;
}
```

will generate

```yaml
components:
    schemas:
        User:
            type:        object
            title:       user model
            description: user model description
            properties:
                name:
                    type: string
                city:
                    type:
                        - string
                        - 'null'
                contact:
                    $ref: '#/components/schemas/Contact'
            required:
                - name
                - contact
        Contact:
            type:        object
            title:       contact model
            description: contact model description
            properties:
                phone:
                    type: string
            required:
                - phone
```

