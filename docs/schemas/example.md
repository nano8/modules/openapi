---
layout: doc
---

<script setup>
const args = {
    Example: [
        { name: 'value', type: 'string' },
    ],
};
</script>

##### laylatichy\nano\modules\openapi\components\schemas\Example

## usage

## <Types fn="Example" r="void" :args="args.Example" /> {#Example}

```php
#[Schema('user model', 'user model description')]
class User {
    #[Example('John')]
    public string $name;
}
```

will generate

```yaml
components:
    schemas:
        User:
            type:        object
            title:       user model
            description: user model description
            properties:
                name:
                    type:    string
                    example: John
            required:
                - name
```

