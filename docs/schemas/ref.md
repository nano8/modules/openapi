---
layout: doc
---

<script setup>
const args = {
    Ref: [
        { name: 'value', type: 'string' },
    ],
};
</script>

##### laylatichy\nano\modules\openapi\components\schemas\Ref

## usage

refs on a typed property are set automatically but if you want you can set them manually

## <Types fn="Ref" r="void" :args="args.Ref" /> {#Ref}

```php
#[Schema('contact model', 'contact model description')]
class Contact {}

#[Schema('user model', 'user model description')]
class User {
    #[Ref(Contact::class)]
    public object $contact;
}
```

is equal to 

```php
#[Schema('contact model', 'contact model description')]
class Contact {}

#[Schema('user model', 'user model description')]
class User {
    public Contact $contact;
}
```

both will generate

```yaml
components:
    schemas:
        Contact:
            description: contact model description
            type: object
        User:
            description: user model description
            type: object
            properties:
                contact:
                    $ref: '#/components/schemas/Contact'
```

to set a ref manually to a single property

```php
#[Schema('contact model', 'contact model description')]
class Contact {
    public int $id;
}

#[Schema('user model', 'user model description')]
class User {
    #[Ref(Contact::class, 'id')]
    public int $contactId;
}
```

will generate

```yaml
components:
    schemas:
        Contact:
            description: contact model description
            type:        object
            properties:
                id:
                    type: integer
            required:
                - id
        User:
            description: user model description
            type:        object
            properties:
                contactId:
                    $ref: '#/components/schemas/Contact/properties/id'
            required:
                - contactId
```

