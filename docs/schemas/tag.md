---
layout: doc
---

<script setup>
const args = {
    Tag: [
        { name: 'name', type: 'string' },
    ],
};
</script>

##### laylatichy\nano\modules\openapi\components\schemas\Tag

## usage

## <Types fn="Tag" r="void" :args="args.Tag" /> {#Tag}

```php
#[Schema('user model', 'user model description')]
#[Tag('users')]
#[Tag('v1')]
class User {
    public ?string $name;
}
```

will generate

```yaml
components:
    schemas:
        User:
            type:        object
            title:       user model
            description: user model description
            properties:
                name:
                    type: string
            x-tag:
                -   name: users
                -   name: v1
```

