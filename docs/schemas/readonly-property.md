---
layout: doc
---

<script setup>
const args = {
    ReadonlyProperty: [],
};
</script>

##### laylatichy\nano\modules\openapi\components\schemas\ReadonlyProperty

## usage

## <Types fn="ReadonlyProperty" r="void" :args="args.ReadonlyProperty" /> {#ReadonlyProperty}

```php
#[Schema('user model', 'user model description')]
class User {
    #[ReadonlyProperty()]
    public string $name;
}
```

will generate

```yaml
components:
    schemas:
        User:
            type:        object
            title:       user model
            description: user model description
            properties:
                name:
                    type:     string
                    readOnly: true
            required:
                - name
```

