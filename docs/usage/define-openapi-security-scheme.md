---
layout: doc
---

<script setup>
const args = {
    defineOpenApiSecurityScheme: [
        { name: 'name', type: 'string' },
        { name: 'scheme', type: 'SecurityScheme' },
    ],
};
</script>

##### laylatichy\nano\modules\openapi\OpenApi

## usage

## <Types fn="defineOpenApiSecurityScheme" r="void" :args="args.defineOpenApiSecurityScheme" /> {#defineOpenApiSecurityScheme}

```php
// .config/openapi.php

$bearer = new Bearer();

defineOpenApiSecurityScheme('jwt', $bearer);
```

will generate

```yaml
components:
    securitySchemes:
        jwt:
            type:   http
            scheme: bearer
```

