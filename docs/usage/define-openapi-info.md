---
layout: doc
---

<script setup>
const args = {
    defineOpenApiInfo: [
        { name: 'title', type: 'string' },
        { name: 'description', type: 'string' },
        { name: 'version', type: 'string' },
        { name: 'email', type: 'string' },
    ],
};
</script>

##### laylatichy\nano\modules\openapi\OpenApi

## usage

## <Types fn="defineOpenApiInfo" r="void" :args="args.defineOpenApiInfo" /> {#defineOpenApiInfo}

```php
// .config/openapi.php

defineOpenApiInfo(
    title: 'My API',
    description: 'My API description',
    version: '1.0.0',
    email: 'email@example.com',
);
```

will generate

```yaml
info:
    title:       My API
    description: My API description
    contact:
        email: email@example.com
    version:     1.0.0
```

