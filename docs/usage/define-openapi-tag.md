---
layout: doc
---

<script setup>
const args = {
    defineOpenApiTag: [
        { name: 'name', type: 'string' },
    ],
};
</script>

##### laylatichy\nano\modules\openapi\OpenApi

## usage

## <Types fn="defineOpenApiTag" r="void" :args="args.defineOpenApiTag" /> {#defineOpenApiTag}

```php
// .config/openapi.php

defineOpenApiTag('users');
defineOpenApiTag('posts');
```

will generate

```yaml
tags:
    -   name: users
    -   name: posts
```

