---
layout: doc
---

<script setup>
const args = {
    defineOpenApiServer: [
        { name: 'url', type: 'string' },
        { name: 'description', type: 'string' },
    ],
};
</script>

##### laylatichy\nano\modules\openapi\OpenApi

## usage

## <Types fn="defineOpenApiServer" r="void" :args="args.defineOpenApiServer" /> {#defineOpenApiServer}

```php
// .config/openapi.php

defineOpenApiServer('https://api.staging.example.com', 'example staging server');
defineOpenApiServer('https://api.example.com', 'example production server');
```

will generate

```yaml
servers:
    -   url:         https://api.staging.example.com
        description: example staging server
    -   url:         https://api.example.com
        description: example production server
```

