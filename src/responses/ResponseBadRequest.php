<?php

namespace laylatichy\nano\modules\openapi\responses;

use Attribute;
use JsonSerializable;
use laylatichy\nano\core\httpcode\HttpCode;

#[Attribute(Attribute::TARGET_FUNCTION)]
class ResponseBadRequest implements IResponse, JsonSerializable {
    public HttpCode $httpCode = HttpCode::BAD_REQUEST;

    public function jsonSerialize(): array {
        return [
            'description' => 'bad request',
            'content'     => [
                'application/json' => [
                    'schema' => [
                        'type'       => 'object',
                        'properties' => [
                            'response' => [
                                'type'    => 'string',
                                'example' => 'bad request',
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
