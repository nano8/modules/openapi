<?php

namespace laylatichy\nano\modules\openapi\responses;

use Attribute;
use JsonSerializable;
use laylatichy\nano\core\httpcode\HttpCode;
use laylatichy\nano\modules\openapi\components\schemas\Ref;

#[Attribute(Attribute::TARGET_FUNCTION)]
class Response implements IResponse, JsonSerializable {
    public Ref $ref;

    /**
     * @param class-string $ref
     */
    public function __construct(
        public HttpCode $httpCode,
        string $ref,
        public bool $repository = false,
        public bool $nullable = true,
        public string $description = '',
    ) {
        $this->ref = new Ref($ref);
    }

    public function jsonSerialize(): array {
        $schema = $this->repository
            ? [
                'type'       => 'object',
                'properties' => [
                    'response' => [
                        'type'       => 'object',
                        'properties' => [
                            'data' => [
                                'type'  => 'array',
                                'items' => [
                                    '$ref' => $this->ref,
                                ],
                            ],
                            'prev' => [
                                'type' => 'boolean',
                            ],
                            'next' => [
                                'type' => 'boolean',
                            ],
                        ],
                    ],
                ],
            ]
            : [
                'type'       => 'object',
                'properties' => [
                    'response' => $this->nullable
                        ? [
                            'oneOf' => [
                                [
                                    '$ref' => $this->ref,
                                ],
                                [
                                    'type' => 'null',
                                ],
                            ],
                        ]
                        : [
                            '$ref' => $this->ref,
                        ],
                ],
            ];

        return [
            'description' => $this->description,
            'content'     => [
                'application/json' => [
                    'schema' => $schema,
                ],
            ],
        ];
    }
}
