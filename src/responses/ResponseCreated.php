<?php

namespace laylatichy\nano\modules\openapi\responses;

use Attribute;
use laylatichy\nano\core\httpcode\HttpCode;

#[Attribute(Attribute::TARGET_FUNCTION)]
class ResponseCreated extends Response {
    /**
     * @param class-string $ref
     */
    public function __construct(
        string $ref,
        string $description = '',
    ) {
        parent::__construct(HttpCode::CREATED, $ref, false, false, $description);
    }
}
