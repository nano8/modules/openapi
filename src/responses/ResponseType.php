<?php

namespace laylatichy\nano\modules\openapi\responses;

use Attribute;
use JsonSerializable;
use laylatichy\nano\core\httpcode\HttpCode;

#[Attribute(Attribute::TARGET_FUNCTION)]
class ResponseType implements IResponse, JsonSerializable {
    public function __construct(
        public string $type,
        public string $description = '',
        public HttpCode $httpCode = HttpCode::OK,
    ) {}

    public function jsonSerialize(): array {
        return [
            'description' => $this->description,
            'content'     => [
                'application/json' => [
                    'schema' => [
                        'type'       => 'object',
                        'properties' => [
                            'response' => [
                                'type'    => $this->type,
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
