<?php

namespace laylatichy\nano\modules\openapi\responses;

use Attribute;
use JsonSerializable;
use laylatichy\nano\core\httpcode\HttpCode;

#[Attribute(Attribute::TARGET_FUNCTION)]
class ResponseBool implements IResponse, JsonSerializable {
    public function __construct(
        public HttpCode $httpCode = HttpCode::OK,
        public string $description = '',
    ) {}

    public function jsonSerialize(): array {
        return [
            'description' => $this->description,
            'content'     => [
                'application/json' => [
                    'schema' => [
                        'type'       => 'object',
                        'properties' => [
                            'response' => [
                                'type'    => 'boolean',
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
