<?php

namespace laylatichy\nano\modules\openapi\responses;

use Attribute;
use JsonSerializable;
use laylatichy\nano\core\httpcode\HttpCode;
use laylatichy\nano\modules\openapi\components\schemas\Ref;

#[Attribute(Attribute::TARGET_FUNCTION)]
class ResponseCollection implements IResponse, JsonSerializable {
    public HttpCode $httpCode = HttpCode::OK;

    public Ref $ref;

    /**
     * @param class-string $ref
     */
    public function __construct(
        string $ref,
        public string $description = '',
    ) {
        $this->ref = new Ref($ref);
    }

    public function jsonSerialize(): array {
        return [
            'description' => $this->description,
            'content'     => [
                'application/json' => [
                    'schema' => [
                        'type'       => 'object',
                        'properties' => [
                            'response' => [
                                'type'  => 'array',
                                'items' => [
                                    '$ref' => $this->ref,
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
