<?php

namespace laylatichy\nano\modules\openapi\responses;

use Attribute;
use laylatichy\nano\core\httpcode\HttpCode;

#[Attribute(Attribute::TARGET_FUNCTION)]
class ResponseOK extends Response {
    /**
     * @param class-string $ref
     */
    public function __construct(
        string $ref,
        public bool $repository = false,
        public bool $nullable = true,
        public string $description = '',
    ) {
        parent::__construct(HttpCode::OK, $ref, $repository, $nullable, $description);
    }
}
