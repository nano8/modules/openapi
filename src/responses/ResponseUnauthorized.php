<?php

namespace laylatichy\nano\modules\openapi\responses;

use Attribute;
use JsonSerializable;
use laylatichy\nano\core\httpcode\HttpCode;

#[Attribute(Attribute::TARGET_FUNCTION)]
class ResponseUnauthorized implements IResponse, JsonSerializable {
    public HttpCode $httpCode = HttpCode::UNAUTHORIZED;

    public function jsonSerialize(): array {
        return [
            'description' => 'unauthorized',
            'content'     => [
                'application/json' => [
                    'schema' => [
                        'type'       => 'object',
                        'properties' => [
                            'response' => [
                                'type'    => 'string',
                                'example' => 'unauthorized',
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
