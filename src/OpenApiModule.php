<?php

namespace laylatichy\nano\modules\openapi;

use laylatichy\nano\events\NanoStart;
use laylatichy\nano\modules\NanoModule;
use laylatichy\nano\modules\openapi\listeners\NanoStartListener;
use laylatichy\nano\Nano;

class OpenApiModule implements NanoModule {
    public OpenApi $openapi;

    public function __construct() {
        // nothing to do here
    }

    public function register(Nano $nano): void {
        if (isset($this->openapi)) {
            useNanoException('openapi module already registered');
        }

        $this->openapi = new OpenApi();

        $this->load();

        $this->listeners();
    }

    private function load(): void {
        $file = useConfig()->getRoot() . '/../.config/openapi.php';

        if (file_exists($file)) {
            require $file;
        }
    }

    private function listeners(): void {
        useEvent(NanoStart::class)->attach(new NanoStartListener());
    }
}
