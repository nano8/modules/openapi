<?php

namespace laylatichy\nano\modules\openapi\listeners;

use laylatichy\nano\core\config\Environment;
use laylatichy\nano\events\NanoEventListener;
use laylatichy\nano\events\NanoStartEvent;

class NanoStartListener implements NanoEventListener {
    public function handle(NanoStartEvent $event): void {
        if (useConfig()->getMode() === Environment::DEV) {
            useOpenApi()->generate();
        }
    }
}
