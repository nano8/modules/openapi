<?php

namespace laylatichy\nano\modules\openapi;

use Exception;
use JsonSerializable;
use laylatichy\nano\modules\openapi\components\Components;
use laylatichy\nano\modules\openapi\info\Info;
use laylatichy\nano\modules\openapi\paths\Paths;
use laylatichy\nano\modules\openapi\servers\Server;
use laylatichy\nano\modules\openapi\tags\Tag;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;

class OpenApi implements JsonSerializable {
    public Info $info;

    public array $classes = [];

    /**
     * @param Server[] $servers
     * @param Tag[]    $tags
     */
    public function __construct(
        public string $openapi = '3.1.0',
        public array $servers = [],
        public Paths $paths = new Paths(),
        public Components $components = new Components(),
        public array $tags = [],
    ) {}

    public function withInfo(Info $info): self {
        $this->info = $info;

        return $this;
    }

    public function withServer(Server $server): self {
        $this->servers[] = $server;

        return $this;
    }

    public function withTag(Tag $tag): self {
        $this->tags[] = $tag;

        return $this;
    }

    public function generate(): void {
        $this->collectClasses();

        $this->components->collect($this->classes); // @phpstan-ignore-line

        $this->paths->collect(useRouter()->getRoutes()); // @phpstan-ignore-line

        $yaml = Yaml::dump(
            json_decode(json_encode($this), true), // @phpstan-ignore-line
            20,
            4,
            Yaml::DUMP_EMPTY_ARRAY_AS_SEQUENCE,
        );

        $dir = useConfig()->getRoot() . '/../api';

        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        file_put_contents("{$dir}/openapi.yaml", $yaml);
    }

    public function collectClasses(): void {
        $finder = new Finder();

        try {
            $finder
                ->files()
                ->in(useConfig()->getRoot())
                ->name('*.php')
                ->contains('use laylatichy\nano\modules\openapi\components\schemas\Schema;');

            foreach ($finder as $file) {
                if (file_exists($file->getRealPath())
                    && !in_array($file->getRealPath(), get_included_files(), true)) {
                    require_once $file->getRealPath();
                }
            }
        } catch (Exception $e) {
            // src directory not found
        }

        $this->classes = get_declared_classes();
    }

    public function jsonSerialize(): array {
        return [
            'openapi'    => $this->openapi,
            'info'       => $this->info,
            'servers'    => $this->servers,
            'paths'      => $this->paths,
            'components' => $this->components,
            'tags'       => $this->tags,
        ];
    }
}
