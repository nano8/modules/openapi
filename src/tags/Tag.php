<?php

namespace laylatichy\nano\modules\openapi\tags;

class Tag {
    public function __construct(
        public string $name,
    ) {}
}
