<?php

namespace laylatichy\nano\modules\openapi\paths;

use Attribute;
use JsonSerializable;
use laylatichy\nano\modules\openapi\requests\Body;
use laylatichy\nano\modules\openapi\requests\Parameter;
use laylatichy\nano\modules\openapi\responses\IResponse;

#[Attribute(Attribute::TARGET_FUNCTION)]
class Path implements JsonSerializable {
    /**
     * @param Tag[]                        $tags
     * @param array<int|string, IResponse> $responses
     * @param Security[]                   $security
     * @param Parameter[]                  $parameters
     */
    public function __construct(
        public Summary $summary,
        public array $tags = [],
        public array $responses = [],
        public string $operationId = '',
        public ?Body $requestBody = null,
        public array $parameters = [],
        public array $security = [],
    ) {}

    public function jsonSerialize(): array {
        $data = [
            'summary'     => $this->summary->value,
            'description' => $this->summary->description,
        ];

        if (count($this->responses) > 0) {
            $data['responses'] = $this->responses;
        }

        if (count($this->tags) > 0) {
            $data['tags'] = $this->tags;
        }

        if ($this->requestBody) {
            $data['requestBody'] = $this->requestBody;
        }

        if (count($this->security) > 0) {
            $data['security'] = $this->security;
        }

        if (count($this->parameters) > 0) {
            $data['parameters'] = $this->parameters;
        }

        $data['operationId'] = $this->operationId;

        return $data;
    }
}
