<?php

namespace laylatichy\nano\modules\openapi\paths;

use Attribute;
use JsonSerializable;

#[Attribute(Attribute::TARGET_FUNCTION | Attribute::IS_REPEATABLE)]
class Security implements JsonSerializable {
    public function __construct(
        public string $value,
    ) {}

    public function jsonSerialize(): array {
        return [
            $this->value => [],
        ];
    }
}
