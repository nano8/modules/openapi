<?php

namespace laylatichy\nano\modules\openapi\paths;

enum PathType: string {
    case get     = 'GET';
    case post    = 'POST';
    case put     = 'PUT';
    case patch   = 'PATCH';
    case delete  = 'DELETE';
    case head    = 'HEAD';
    case options = 'OPTIONS';
    case trace   = 'TRACE';
    case connect = 'CONNECT';
}
