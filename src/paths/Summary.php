<?php

namespace laylatichy\nano\modules\openapi\paths;

use Attribute;
use JsonSerializable;

#[Attribute(Attribute::TARGET_FUNCTION)]
class Summary implements JsonSerializable {
    public function __construct(
        public string $value,
        public string $description,
    ) {}

    public function jsonSerialize(): array {
        return [
            'summary'     => $this->value,
            'description' => $this->description,
        ];
    }
}
