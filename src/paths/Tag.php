<?php

namespace laylatichy\nano\modules\openapi\paths;

use Attribute;
use JsonSerializable;

#[Attribute(Attribute::TARGET_FUNCTION | Attribute::IS_REPEATABLE)]
class Tag implements JsonSerializable {
    public function __construct(
        public string $value,
    ) {}

    public function jsonSerialize(): string {
        return $this->value;
    }
}
