<?php

namespace laylatichy\nano\modules\openapi\paths;

use JsonSerializable;
use laylatichy\nano\core\request\Request;
use laylatichy\nano\core\router\Callback;
use laylatichy\nano\modules\openapi\components\schemas\PropertyType;
use laylatichy\nano\modules\openapi\requests\Body;
use laylatichy\nano\modules\openapi\requests\Parameter;
use laylatichy\nano\modules\openapi\responses\Response;
use laylatichy\nano\modules\openapi\responses\ResponseBadRequest;
use laylatichy\nano\modules\openapi\responses\ResponseBool;
use laylatichy\nano\modules\openapi\responses\ResponseCollection;
use laylatichy\nano\modules\openapi\responses\ResponseCreated;
use laylatichy\nano\modules\openapi\responses\ResponseOK;
use laylatichy\nano\modules\openapi\responses\ResponseType;
use laylatichy\nano\modules\openapi\responses\ResponseUnauthorized;
use ReflectionFunction;
use ReflectionNamedType;

class Paths implements JsonSerializable {
    /**
     * @param array<int|string, array<string, Path>> $paths
     */
    public function __construct(
        public array $paths = [],
    ) {}

    /**
     * @param array<string, array<string, Callback>> $routes
     */
    public function collect(array $routes): void {
        foreach ($routes as $method => $paths) {
            foreach ($paths as $path => $callback) {
                $reflection = new ReflectionFunction($callback->callback);

                // get summary attribute
                $summaryAttribute = $reflection->getAttributes(Summary::class);

                if (count($summaryAttribute) === 0) {
                    continue;
                }

                $summary = $summaryAttribute[0]->newInstance();

                $tags = [];

                foreach ($reflection->getAttributes(Tag::class) as $tagAttribute) {
                    $tags[] = $tagAttribute->newInstance();
                }

                $type = PathType::from($method);

                $path_parsed = preg_replace('/\{([^:}]+):[^}]+}/', '{$1}', stripslashes($path));

                $operationId = rtrim(
                    // @phpstan-ignore-next-line
                    preg_replace(
                        '/-+/',
                        '-',
                        // @phpstan-ignore-next-line
                        preg_replace(
                            '/[^a-z0-9]/',
                            '-',
                            strtolower("{$type->name}-{$path_parsed}")
                        )
                    ),
                    '-'
                );

                $responses = [];

                $responseAttributes = [
                    ...$reflection->getAttributes(Response::class),
                    ...$reflection->getAttributes(ResponseOK::class),
                    ...$reflection->getAttributes(ResponseBadRequest::class),
                    ...$reflection->getAttributes(ResponseUnauthorized::class),
                    ...$reflection->getAttributes(ResponseCreated::class),
                    ...$reflection->getAttributes(ResponseBool::class),
                    ...$reflection->getAttributes(ResponseType::class),
                    ...$reflection->getAttributes(ResponseCollection::class),
                ];

                foreach ($responseAttributes as $responseAttribute) {
                    $response = $responseAttribute->newInstance();

                    $responses[$response->httpCode->code()] = $response;
                }

                $requestBody = null;

                $bodyAttribute = $reflection->getAttributes(Body::class);

                if (count($bodyAttribute) > 0) {
                    $requestBody = $bodyAttribute[0]->newInstance();
                }

                $security = [];

                foreach ($reflection->getAttributes(Security::class) as $securityAttribute) {
                    $security[] = $securityAttribute->newInstance();
                }

                $parameters = [];

                foreach ($reflection->getParameters() as $parameter) {
                    if ($parameter->getType() instanceof ReflectionNamedType) {
                        // if param is Request, skip it
                        if ($parameter->getType()->getName() === Request::class) {
                            continue;
                        }

                        $parameters[] = new Parameter(
                            $parameter->getName(),
                            PropertyType::fromReflectionType($parameter->getType()),
                            !$parameter->isOptional(),
                        );
                    }
                }
                
                $this->paths[$path_parsed][$type->name] = new Path(
                    $summary,
                    $tags,
                    $responses,
                    $operationId,
                    $requestBody,
                    $parameters,
                    $security,
                );
            }
        }
    }

    public function jsonSerialize(): array {
        return $this->paths;
    }
}
