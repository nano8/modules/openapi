<?php

namespace laylatichy\nano\modules\openapi\info;

class Info {
    public function __construct(
        public string $title,
        public string $description,
        public Contact $contact,
        public string $version,
    ) {}
}
