<?php

namespace laylatichy\nano\modules\openapi\info;

class Contact {
    public function __construct(
        public string $email,
    ) {}
}
