<?php

namespace laylatichy\nano\modules\openapi\servers;

class Server {
    public function __construct(
        public string $url,
        public string $description,
    ) {}
}
