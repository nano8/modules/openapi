<?php

namespace laylatichy\nano\modules\openapi\components;

use laylatichy\nano\modules\openapi\components\schemas\Schema;
use laylatichy\nano\modules\openapi\components\securitySchemes\SecurityScheme;

class Components {
    /**
     * @param array<string, Schema>         $schemas
     * @param array<string, SecurityScheme> $securitySchemes
     */
    public function __construct(
        public array $schemas = [],
        public array $securitySchemes = [],
    ) {}

    public function withSecurityScheme(string $name, SecurityScheme $securityScheme): void {
        $this->securitySchemes[$name] = $securityScheme;
    }

    /**
     * @param class-string[] $classes
     */
    public function collect(array $classes): void {
        $this->schemas = Schema::collect($classes);
    }
}
