<?php

namespace laylatichy\nano\modules\openapi\components\schemas;

use Attribute;
use JsonSerializable;
use ReflectionClass;

#[Attribute(Attribute::TARGET_PROPERTY)]
class Ref implements JsonSerializable {
    public string $ref;

    /**
     * @param class-string $class
     */
    public function __construct(
        string $class,
        ?string $property = null,
    ) {
        $reflection = new ReflectionClass($class);

        $this->ref = "#/components/schemas/{$reflection->getShortName()}";

        if ($property !== null) {
            $this->ref .= "/properties/{$property}";
        }
    }

    public function jsonSerialize(): string {
        return $this->ref;
    }
}
