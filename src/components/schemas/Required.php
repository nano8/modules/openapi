<?php

namespace laylatichy\nano\modules\openapi\components\schemas;

use Attribute;
use JsonSerializable;
use ReflectionClass;

#[Attribute(Attribute::TARGET_PROPERTY)]
class Required implements JsonSerializable {
    public string $name;

    public function __construct(
        public bool $required = true,
    ) {}

    /**
     * @param ReflectionClass<object> $class
     *
     * @return self[]
     */
    public static function collect(ReflectionClass $class): array {
        $required = [];

        $properties = $class->getProperties();

        foreach ($properties as $property) {
            // if it's a parent property then skip
            if ($property->getDeclaringClass()->getName() !== $class->getName()) {
                continue;
            }

            $internal = $property->getAttributes(Internal::class);

            if (count($internal) > 0) {
                continue;
            }

            $attributes = $property->getAttributes(self::class);

            if (count($attributes) === 0) {
                // check if is nullable, if so, skip
                if ($property->getType()?->allowsNull()) {
                    continue;
                }

                $attribute       = new self();
                $attribute->name = $property->getName();
                $required[]      = $attribute;
            } else {
                $attribute = $attributes[0]->newInstance();

                if ($attribute->required) {
                    $attribute->name = $property->getName();
                    $required[]      = $attribute;
                }
            }
        }

        return $required;
    }

    public function jsonSerialize(): string {
        return $this->name;
    }
}
