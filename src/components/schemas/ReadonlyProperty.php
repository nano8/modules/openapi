<?php

namespace laylatichy\nano\modules\openapi\components\schemas;

use Attribute;
use ReflectionProperty;

#[Attribute(Attribute::TARGET_PROPERTY)]
class ReadonlyProperty {
    public function __construct() {}

    public static function collect(ReflectionProperty $property): bool {
        $attributes = $property->getAttributes(self::class);

        if (count($attributes) === 0) {
            return false;
        }

        return true;
    }
}
