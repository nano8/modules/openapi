<?php

namespace laylatichy\nano\modules\openapi\components\schemas;

use Attribute;

#[Attribute(Attribute::TARGET_PROPERTY)]
class Internal {
    public function __construct() {}
}
