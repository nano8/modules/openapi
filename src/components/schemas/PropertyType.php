<?php

namespace laylatichy\nano\modules\openapi\components\schemas;

use ReflectionNamedType;

enum PropertyType: string {
    case string  = 'string';
    case integer = 'integer';
    case boolean = 'boolean';
    case array   = 'array';
    case null    = 'null';
    case ref     = 'ref';

    public static function fromReflectionType(ReflectionNamedType $type): self {
        return match ($type->getName()) {
            'string' => self::string,
            'int'    => self::integer,
            'bool'   => self::boolean,
            'array'  => self::array,
            'null'   => self::null,
            default  => self::ref,
        };
    }
}
