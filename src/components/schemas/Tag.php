<?php

namespace laylatichy\nano\modules\openapi\components\schemas;

use Attribute;
use ReflectionClass;

#[Attribute(Attribute::TARGET_CLASS | Attribute::IS_REPEATABLE)]
class Tag {
    public function __construct(public string $name) {}

    /**
     * @param ReflectionClass<object> $class
     *
     * @return self[]
     */
    public static function collect(ReflectionClass $class): array {
        $attributes = $class->getAttributes(self::class);

        if (count($attributes) === 0) {
            return [];
        }

        $tags = [];

        foreach ($attributes as $attribute) {
            $tags[] = $attribute->newInstance();
        }

        return $tags;
    }
}
