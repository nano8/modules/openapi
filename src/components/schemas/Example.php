<?php

namespace laylatichy\nano\modules\openapi\components\schemas;

use Attribute;
use JsonSerializable;
use ReflectionProperty;

#[Attribute(Attribute::TARGET_PROPERTY)]
class Example implements JsonSerializable {
    public function __construct(
        public array|int|string $value,
    ) {}

    public static function collect(ReflectionProperty $property): ?self {
        $attributes = $property->getAttributes(self::class);

        if (count($attributes) === 0) {
            return null;
        }

        return $attributes[0]->newInstance();
    }

    public function jsonSerialize(): array|int|string {
        return $this->value;
    }
}
