<?php

namespace laylatichy\nano\modules\openapi\components\schemas;

use Attribute;
use JsonSerializable;
use laylatichy\nano\modules\orm\Model;
use ReflectionClass;

#[Attribute(Attribute::TARGET_CLASS)]
class Schema implements JsonSerializable {
    public string $type = 'object';

    public ?string $title;

    public ?string $description;

    /**
     * @param array<string, Property> $properties
     * @param Required[]              $required
     * @param Tag[]                   $tags
     */
    public function __construct(
        ?string $title = null,
        ?string $description = null,
        public array $properties = [],
        public array $required = [],
        public array $tags = [],
    ) {
        $this->title       = $title;
        $this->description = $description;
    }

    /**
     * @param class-string[] $classes
     *
     * @return array<string, self>
     */
    public static function collect(array $classes): array {
        $schemas = [];

        // get all classes with Schema attribute
        foreach ($classes as $class) {
            $reflection = new ReflectionClass($class);
            $attributes = $reflection->getAttributes(self::class);

            if (count($attributes) === 0) {
                continue;
            }

            if (count($attributes[0]->getArguments()) === 0) {
                $title = $reflection->getShortName();

                $schema = new self($title, $class);
            } else {
                $schema = $attributes[0]->newInstance();
            }

            $collect = [
                $reflection,
            ];

            $name = $reflection->getShortName();

            $is_model = false;

            while ($reflection->getParentClass()) {
                if ($reflection->getParentClass()->getName() === Model::class) {
                    $is_model = true;

                    break;
                }
                $collect[]  = $reflection->getParentClass();
                $reflection = $reflection->getParentClass();
                
            }

            if ($is_model && !isset($schema->properties['id'])) {
                $schema->properties['id'] = new Property(
                    'id',
                    PropertyType::integer,
                    new Format('int64'),
                    new Example(1),
                    null,
                    true,
                );
            }

            foreach ($collect as $item) {
                $schema->tags       = array_merge($schema->tags, Tag::collect($item));
                $schema->required   = array_merge($schema->required, Required::collect($item));
                $schema->properties = array_merge($schema->properties, Property::collect($item));
            }

            if ($is_model) {
                if (!isset($schema->properties['created_at'])) {
                    $schema->properties['created_at'] = new Property(
                        'created_at',
                        PropertyType::string,
                        new Format('date-time'),
                        new Example('2000-01-01 00:00:00'),
                        null,
                        true,
                    );
                }

                if (!isset($schema->properties['updated_at'])) {
                    $schema->properties['updated_at'] = new Property(
                        'updated_at',
                        PropertyType::string,
                        new Format('date-time'),
                        new Example('2000-01-01 00:00:00'),
                        null,
                        true,
                    );
                }

                if (!isset($schema->properties['source'])) {
                    $schema->properties['source'] = new Property(
                        'source',
                        PropertyType::string,
                        new Format('string'),
                        new Example('redis'),
                        null,
                        true,
                    );
                }
            }

            $schemas[$name] = $schema;
        }

        return $schemas;
    }

    public function jsonSerialize(): array {
        return [
            'type'        => $this->type,
            'title'       => $this->title,
            'description' => $this->description,
            'properties'  => $this->properties,
            'required'    => $this->required,
            'x-tags'      => $this->tags,
        ];
    }
}
