<?php

namespace laylatichy\nano\modules\openapi\components\schemas;

use JsonSerializable;
use ReflectionClass;
use ReflectionEnum;
use ReflectionNamedType;

class Property implements JsonSerializable {
    public function __construct(
        public string $name,
        public PropertyType $type,
        public ?Format $format = null,
        public ?Example $example = null,
        public ?Ref $ref = null,
        public bool $readOnly = false,
        public bool $nullable = false,
        public ?Description $description = null,
    ) {}

    /**
     * @param ReflectionClass<object> $class
     *
     * @return array<string, self>
     */
    public static function collect(ReflectionClass $class): array {
        $data = [];

        foreach ($class->getProperties() as $property) {
            // if it's a parent property then skip
            if ($property->getDeclaringClass()->getName() !== $class->getName()) {
                continue;
            }

            if (count($property->getAttributes(Internal::class)) > 0) {
                continue;
            }

            if ($property->getType() instanceof ReflectionNamedType) {
                $type = PropertyType::fromReflectionType($property->getType());

                $obj = new self($property->getName(), $type);

                $attributes = $property->getAttributes(Ref::class);

                if ($type === PropertyType::ref || count($attributes) > 0) {
                    $target = null;

                    if (!$property->getType()->isBuiltin()) {
                        /** @var class-string $property_class */
                        $property_class = $property->getType()->getName();
                        $target         = new ReflectionClass($property_class);
                    }

                    if ($target?->isEnum()) {
                        // @phpstan-ignore-next-line
                        $enum      = new ReflectionEnum($target->getName());

                        if ($enum->getBackingType()) {
                            $type = PropertyType::fromReflectionType($enum->getBackingType());

                            $obj->type = $type;
                        }
                    } else {
                        $obj->ref = count($attributes) === 0
                            // @phpstan-ignore-next-line
                            ? new Ref($property->getType()->getName())
                            : $attributes[0]->newInstance();
                    }
                }

                $obj->format      = Format::collect($property);
                $obj->example     = Example::collect($property);
                $obj->readOnly    = ReadonlyProperty::collect($property);
                $obj->nullable    = $property->getType()->allowsNull();
                $obj->description = Description::collect($property);

                $data[$property->getName()] = $obj;
            }
        }

        return $data;
    }

    public function jsonSerialize(): array {
        if ($this->ref) {
            $data = [
                '$ref' => $this->ref,
            ];

            if ($this->example) {
                $data['example'] = $this->example;
            }

            if ($this->readOnly) {
                $data['readOnly'] = $this->readOnly;
            }

            if ($this->description) {
                $data['description'] = $this->description;
            }

            return $data;
        }

        $data = [];

        $data['type'] = $this->nullable
            ? [
                $this->type,
                PropertyType::null,
            ]
            : $this->type;

        if ($this->format) {
            $data['format'] = $this->format;
        }

        if ($this->example) {
            $data['example'] = $this->example;
        }

        if ($this->readOnly) {
            $data['readOnly'] = $this->readOnly;
        }

        if ($this->description) {
            $data['description'] = $this->description;
        }

        return $data;
    }
}
