<?php

namespace laylatichy\nano\modules\openapi\components\schemas;

use Attribute;
use JsonSerializable;
use ReflectionProperty;

#[Attribute(Attribute::TARGET_PROPERTY)]
class Description implements JsonSerializable {
    public function __construct(
        public string $value,
    ) {}

    public static function collect(ReflectionProperty $property): ?self {
        $attributes = $property->getAttributes(self::class);

        if (count($attributes) === 0) {
            return null;
        }

        return $attributes[0]->newInstance();
    }

    public function jsonSerialize(): string {
        return $this->value;
    }
}
