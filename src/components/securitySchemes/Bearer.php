<?php

namespace laylatichy\nano\modules\openapi\components\securitySchemes;

class Bearer implements SecurityScheme {
    public string $type   = 'http';

    public string $scheme = 'bearer';
}
