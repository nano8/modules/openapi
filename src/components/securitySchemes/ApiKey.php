<?php

namespace laylatichy\nano\modules\openapi\components\securitySchemes;

class ApiKey implements SecurityScheme {
    public string $type = 'apiKey';

    public string $in   = 'header';

    public function __construct(
        public string $name,
    ) {}
}
