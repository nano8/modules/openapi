<?php

namespace laylatichy\nano\modules\openapi\requests;

use JsonSerializable;
use laylatichy\nano\modules\openapi\components\schemas\PropertyType;

class Parameter implements JsonSerializable {
    public function __construct(
        public string $name,
        public PropertyType $type,
        public bool $required = true,
    ) {}

    public function jsonSerialize(): array {
        return [
            'name'        => $this->name,
            'in'          => 'path',
            'required'    => $this->required,
            'schema'      => [
                'type' => $this->type,
            ],
        ];
    }
}
