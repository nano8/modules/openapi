<?php

namespace laylatichy\nano\modules\openapi\requests;

use Attribute;
use JsonSerializable;
use laylatichy\nano\modules\openapi\components\schemas\Property;
use laylatichy\nano\modules\openapi\components\schemas\Ref;
use laylatichy\nano\modules\openapi\components\schemas\Required;
use ReflectionClass;

#[Attribute(Attribute::TARGET_FUNCTION)]
class Body implements JsonSerializable {
    public ?Ref $ref        = null;

    public array $properties = [];

    /**
     * @var Required[]
     */
    public array $required = [];

    /**
     * @param class-string $class
     */
    public function __construct(string $class, bool $ref = false) {
        if ($ref) {
            $this->ref = new Ref($class);
        } else {
            $reflection = new ReflectionClass($class);

            $this->required = Required::collect($reflection);

            $this->properties = Property::collect($reflection);
        }
    }

    public function jsonSerialize(): array {
        $schema = $this->ref
            ? [
                '$ref' => $this->ref,
            ]
            : [
                'type'       => 'object',
                'properties' => $this->properties,
                'required'   => $this->required,
            ];

        return [
            'content' => [
                'application/json' => [
                    'schema' => $schema,
                ],
            ],
        ];
    }
}
