<?php

use laylatichy\nano\modules\openapi\components\securitySchemes\SecurityScheme;
use laylatichy\nano\modules\openapi\info\Contact;
use laylatichy\nano\modules\openapi\info\Info;
use laylatichy\nano\modules\openapi\OpenApi;
use laylatichy\nano\modules\openapi\OpenApiModule;
use laylatichy\nano\modules\openapi\servers\Server;
use laylatichy\nano\modules\openapi\tags\Tag;

if (!function_exists('defineOpenApiInfo')) {
    function defineOpenApiInfo(
        string $title,
        string $description,
        string $version,
        string $email,
    ): void {
        $contact = new Contact($email);

        $info = new Info($title, $description, $contact, $version);

        useNanoModule(OpenApiModule::class)
            ->openapi
            ->withInfo($info);
    }
}

if (!function_exists('defineOpenApiServer')) {
    function defineOpenApiServer(
        string $url,
        string $description,
    ): void {
        $server = new Server($url, $description);

        useNanoModule(OpenApiModule::class)
            ->openapi
            ->withServer($server);
    }
}

if (!function_exists('defineOpenApiTag')) {
    function defineOpenApiTag(string $name): void {
        $tag = new Tag($name);

        useNanoModule(OpenApiModule::class)
            ->openapi
            ->withTag($tag);
    }
}

if (!function_exists('defineOpenApiSecurityScheme')) {
    function defineOpenApiSecurityScheme(string $name, SecurityScheme $scheme): void {
        useNanoModule(OpenApiModule::class)
            ->openapi
            ->components
            ->withSecurityScheme($name, $scheme);
    }
}

if (!function_exists('useOpenApi')) {
    function useOpenApi(): OpenApi {
        return useNanoModule(OpenApiModule::class)
            ->openapi;
    }
}